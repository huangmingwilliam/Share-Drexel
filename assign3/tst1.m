%Assignment 3 
function assign3()
infinity=1e6;
x=linspace(0,infinity,15);
solinit=bvpinit(x, [0,0,1.1,0]);
sol=bvp4c(@myode,@bcs, solinit);

function dydx = myode(x,y)
dydx=[y(1);y(2);y(3);-y(3)*y(1)];
function res = bcs(ya,yb)
res = [ ya(1);ya(2);yb(2)-2;ya(4)];

% function tst()
% y0=[0.1;0;0];
% [x,y]=ode45('myode',[0,50],y0);
% y

% % ode45 method
% % function tst()
% y0=[0;0;2];
% [x,y]=ode45('tst1',[0,1],y0);
% plot(x,y(:,1))
% % 
% function dydx=tst1(x,y)
% dydx = [y(1);y(2);y(3);-y(1)*y(3)];
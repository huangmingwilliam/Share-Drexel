function f=odefun(t,x)
c=1;g=1;
f(1)=x(2);
f(2)=c*x(2)+g*x(4)-x(1);
f(3)=x(4);
f(4)=c*x(4)-g*x(2)-x(3);
f=f';
end

% [t,y]=ode45(@odefun,[0,10],[0,0.4,0.2,0.3]);
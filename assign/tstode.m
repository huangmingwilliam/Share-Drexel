

function dydx=ivode(x,y)
dydx=[y(3);
    -y(4);
    -exp(-x)+2*y(1)*y(2)/(2*y(2)+y(1));
    exp(-x)-2*y(1)*y(2)/(2*y(2)+y(1))];

function res=ivbc(ya,yb)
res=[ya(1)-1;
    ya(4);
    yb(3);
    ya(3)-yb(4)];
end
solinit=bvpinit(linspace(0,1,10),[1 1 0 0]);
sol=bvp4c(@ivode,@ivbc,solinit);


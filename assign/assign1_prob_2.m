Rp = 3;
R = 6;
b=0.1;
k=25;
Tp = 260;
Ta = 60;
h=30;
alpha = sqrt(2*h/(b*k));
syms y(x)
Dy = diff(y);
Dy2 = diff(y,x,2);

ode = x*diff(y,x,2) + Dy - alpha^2*R^2*y*x == 0;
cond0 = y(0.5) == 1;
% case I
cond1 = y(1) == 0;
conds = [cond0 cond1];
ySol(x) = dsolve(ode, conds);
ySol = simplify(ySol)
x=linspace(0.5,1).';
y=ySol(x);%.*(Ta-Tp)+Ta;
figure(1)
plot(x,y)
% %case II
% cond2 = Dy(1) == 0;
% conds = [cond0 cond2];
% ySoll(x) = dsolve(ode, conds);
% ySoll = simplify(ySol)
% x=linspace(0.5,1).';
% y=ySoll(x);%.*(Ta-Tp)+Ta;
% figure(2)
% plot(x,y)
% 
% %case III
% cond3 = Dy(1) == 216*y(1);
% [x,y]=solution(h, cond1);
% figure(1)
% plot(x,y)



phi=1
x=linspace(0,1).';
delta_x=0.0101;
N=size(x);
N=N(1,1);
z=zeros(N,3);

for i = 1:3
    phi=(10)^(i-2)
   
syms y(x)
Dy = diff(y);
ode = diff(y,x,2) == (phi*phi)*y;
cond1 = y(1) == 1;
cond2 = Dy(0) == 0;
conds = [cond1 cond2];
ySol(x) = dsolve(ode,conds);
ySol = simplify(ySol)
x=linspace(0,1).';
z(:,i) = ySol(x);

end

plot(x,z);

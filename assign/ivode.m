function dydx=ivode(x,y)
dydx=[y(3);
    -y(4);
    -exp(-x)+2*y(1)*y(2)/(2*y(2)+y(1));
    exp(-x)-2*y(1)*y(2)/(2*y(2)+y(1))];


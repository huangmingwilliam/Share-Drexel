syms y(x)
D2y = diff(y,x,2);
Dy = diff(y,x);
eqn2=D2y+(1/x)*Dy-(16*.5)^2*y;
inits2=[y(0)==0 Dy(1)== -h*R*y(1)/k];
y=dsolve(eqn2,inits2,x) % solve above commands in command window to get bessel function equation as listed below (highlighted)
x=linspace(0.5,1).';
%y0=zeros(100,1);
%Rp = 0.25; R = 0.5; b = 1/120; h = 30; k = 25; Tp = 260; Ta = 60;
y=(besseli(0, alpha/2)*besselk(0, (alpha*x)/2))/(besseli(0, alpha/2)*besselk(0, alpha/4) - besseli(0, alpha/4)*besselk(0, alpha/2)) - (besselk(0, alpha/2)*besseli(0, (alpha*x)/2))/(besseli(0, alpha/2)*besselk(0, alpha/4) - besseli(0, alpha/4)*besselk(0, alpha/2));
plot(x,y);
title('Graph of case 1')
xlabel('r/R') % x-axis label
ylabel('Theeta') % y-axis label

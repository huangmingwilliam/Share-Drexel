function [x,y] = solution(h, cond2)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Rp = 3;
R = 6;
b=0.1;
k=25;
Tp = 260;
Ta = 60;
alpha = sqrt(2*h/(b*k));

syms y(x)
ode = x*diff(y,x,2) + 1./x .*diff(y,x)-alpha^2*R^2*y == 0;
cond1 = y(0.5) == -1;
conds = [cond1, cond2];

ySol(x) = dsolve(ode, conds);
ySol = simplify(ySol)

x=linspace(0,1).';
y=ySol(x)*(Ta-Tp) +Ta;
end


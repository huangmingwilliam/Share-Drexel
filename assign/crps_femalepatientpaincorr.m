function r = crps_femalepatientpaincorr(factor, file)
%draw data, get correlation

%filename = '123.xlsx';
[~,~,raw] = xlsread(file);
[x,~] = find(strcmp(raw,'F'));
filtM = raw(x,:);
tmp = find([filtM{:,7}]>0); % get rid of 0
filtM = filtM(tmp,:);
[~, y_f] = find(strcmp(raw(1,:),factor));
[~, y_ff] = find(strcmp(raw(1,:),'Pain'));
x_data = cell2mat(filtM(:,y_ff));
y_data = cell2mat(filtM(:,y_f));
r=corr2(x_data,y_data);
plt_method(x_data,y_data);



function plt_method(XData, YData)
%plot data
plot(XData,YData,'o');
xlabel('');
ylabel('');
title('');

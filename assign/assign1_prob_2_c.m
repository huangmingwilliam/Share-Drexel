%Problem 2(3)
Rp = 3;
R = 6;
b=0.1;
h=[5, 10, 15, 20];
k=25;
Tp = 260;
Ta = 60;
alpha = sqrt(2*h/(b*k));

x=linspace(0,1).';
N=size(x);
N=N(1,1);
y=zeros(N,3);



% Case I
syms theta(tau)
cond2 = theta(1) == 0;
for i = 1:4
    h_tmp = h(i)
[x1,z1]=profile(cond2, h_tmp);
y1(:,i) = z1;

end
figure(1)
plot(x1,y1)

% Case II
syms theta(tau)
Dy = diff(theta, tau);
cond2 = Dy(1) == 0;
for i = 1:4
    h_tmp = h(i)
[x2,z2]=profile(cond2, h_tmp);
y2(:,i) = z2;

end
figure(2)
plot(x2,y2)

% Case III
syms theta(tau)
Dy = diff(theta, tau);
cond2 = Dy(1) == 216*theta(1);
for i = 1:4
    h_tmp = h(i)
[x3,z3]=profile(cond2, h_tmp);
y3(:,i) = z3;

end
figure(3)
plot(x3,y3)

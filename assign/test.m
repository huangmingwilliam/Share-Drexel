%dy/dx = 1;
syms y(x)
cond1 = y(0) == 0;
cond2 = y(1) == 1;
conds = [cond1, cond2];
ode = diff(y,x,2) == 1;
y(x) = dsolve(ode, conds)
x=linspace(0,1).';
y=y(x);
plot(x,y);

function  dYdx = caseIIIODE(x, Y)
gamma = 17.6;
beta = -0.056;

dYdx=[Y(2);
    2*Y(2)+2*3.36*exp(gamma-gamma*Y(3)^-1);
    Y(4);
    2*Y(4)+2*-0.056*3.36*exp(gamma-gamma*Y(3)^-1)];

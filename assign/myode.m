%Assignment2 
function myode()
figure(1)
subplot(2,1,1);
[x,y1,y2] = caseI_ana();
plot(x,y1)
title('Case I: analytical and numerical solution C(z)');
hold on
plot(x,y2)
hold on

x=linspace(0,1,50);
solinit = bvpinit(x,[0.1,0.1]);
[x1,y1,x2,y2]=case_num(solinit, @deriv1_1, @bcs1, @deriv1_15, @bcs2);       %case I
[x3,y3,x4,y4]=case_num(solinit, @deriv2_1, @bcs2_1, @deriv2_15, @bcs2_15);  %case II 
[x5,y5_1,y5_2]=case_numI(@deriv3_1, @bcs3_1);  %case III
[x6,y6_1, y6_2]=case_numIV(@deriv4_1, @bcs4_1, 20);  %case IV
double(y6_1)
double(y6_2)

plot(x1,y1,'x');
hold on
plot(x2,y2,'o');
h=legend(['ana: Pem=1'],['ana: Pem=15'],['num: Pem=1'],['num: Pem=15']);
set(h, 'Box', 'off');
xlabel('z')
ylabel('C')

subplot(2,1,2);
plot(x3,y3,'b-x');
title('Case II: numerical solution C(z)');
hold on
plot(x4,y4,'b-o');
h=legend(['Pem=1'],['Pem=15']);
set(h, 'Box', 'off');
xlabel('z');
ylabel('C');
% 
figure(2)
subplot(2,1,1);
plot(x5,y5_1,'b-x');
title('Case III: numerical solution C(z) and T(z)');
hold on
plot(x5, y5_2,'b-o');
h=legend(['CS'],['T']);
set(h, 'Box', 'off');
xlabel('z');
ylabel('C & T');

subplot(2,1,2);
plot(x6,y6_1,'b-x');
title('Case IV: numerical solution C(z) and T(z)');
hold on
plot(x6, y6_2,'b-o');
h=legend(['C'],['T']);
set(h, 'Box', 'off');
xlabel('z');
ylabel('C & T');

    
function [z,y1,y2] = caseI_ana() %case I analytical solution
ode1 = 'D2C - DC - 8*C = 0';
ode2 = 'D2C - 15*DC - 15*8*C = 0';

cond1 = 'DC(0) = C(0)-1';
cond2 = 'DC(0) = 15*C(0)-15'
cond3 = 'DC(1) = 0';
[C] = dsolve(ode1, cond1, cond3, 'z');CSol = simplify(C)
[C2] = dsolve(ode2, cond2, cond3, 'z');C2Sol = simplify(C2)
z=linspace(0,1).';
y1=subs(CSol);
y2=subs(C2Sol);

function [x1,y1,x2,y2] = case_num(solinit, ode1, bcs1, ode2, bcs2)
% sol1 = bvp4c(@deriv1_1, @bcs1, solinit);
% sol2 = bvp4c(@deriv1_15, @bcs2, solinit);
sol1 = bvp4c(ode1, bcs1, solinit);
sol2 = bvp4c(ode2, bcs2, solinit);
x1=sol1.x;
x2=sol2.x;
y1=sol1.y(1,:);
y2=sol2.y(1,:);

function [x1,y1,y2] = case_numI(ode1, bcs1)
x=linspace(0,1,20);
solinit = bvpinit(x,[0,0.1,0,0.1]);
options = bvpset('AbsTol', 10^(-2), 'Nmax', 20, 'Stats','on');

sol1 = bvp4c(ode1, bcs1, solinit, options);
x1=sol1.x;
y1=sol1.y(1,:);
y2=sol1.y(3,:);

function [x1,y1,y2] = case_numIV(ode1, bcs1, k)
x=linspace(0,1,20);
solinit = bvpinit(x,[0,0.8,0,0.5]);
options = bvpset('AbsTol', 10^(-3), 'Nmax', k, 'Stats','on');

sol1 = bvp4c(ode1, bcs1, solinit, options);
x1=sol1.x;
y1=sol1.y(1,:);
y2=sol1.y(3,:);

function dYdx = deriv1_1(x,Y)

dYdx(1) = Y(2);
dYdx(2) = Y(2)+8*Y(1);

function res = bcs1(ya,yb)
res = [ ya(2) - ya(1) + 1
        yb(2)]; 

function dYdx = deriv1_15(x,Y)

dYdx(1) = Y(2);
dYdx(2) = 15*Y(2)+15*8*Y(1);

function res = bcs2(ya,yb)
res = [ ya(2) - 15*ya(1) + 15
        yb(2)]; 

function dYdx = deriv2_1(x,Y)

dYdx(1) = Y(2);
dYdx(2) = Y(2)+2*Y(1)^2;

function res = bcs2_1(ya,yb)
res = [ ya(2) - ya(1) + 1
        yb(2)]; 
    
function dYdx = deriv2_15(x,Y)

dYdx(1) = Y(2);
dYdx(2) = 15*Y(2)+15*2*Y(1)^2;

function res = bcs2_15(ya,yb)
res = [ ya(2) - 15*ya(1) + 15
        yb(2)]; 
%Case III
function dYdx = deriv3_1(x,Y)
gamma = 17.6;
beta = -0.056;
Pe = 2;
dYdx=[Y(2);
    Pe*Y(2)+Pe*3.36*exp(gamma-gamma*Y(3)^-1);
    Y(4);
    Pe*Y(4)+Pe*beta*3.36*exp(gamma-gamma*Y(3)^-1)];

function res = bcs3_1(ya,yb)
Pe = 2;
res=[ya(2)-Pe*ya(1)+Pe*1;
    yb(2);
    ya(4)-Pe*ya(3)+Pe*1;
    yb(4)]; 

% Case IV
function dYdx = deriv4_1(x,Y)
gamma = 17.6;
beta = -0.056;
Pe = 96;
dYdx=[Y(2);
    Pe*Y(2)+Pe*3.8170*exp(gamma-gamma*Y(3)^-1);
    Y(4);
    Pe*Y(4)+Pe*beta*3.8170*exp(gamma-gamma*Y(3)^-1)];

function res = bcs4_1(ya,yb)
Pe = 96;
res=[ya(2)-Pe*ya(1)+Pe*1;
    yb(2);
    ya(4)-Pe*ya(3)+Pe*1;
    yb(4)]; 
    
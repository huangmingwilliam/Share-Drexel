function [x,z] = profile(cond2,h)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

Rp = 3;
R = 6;
b=0.1;
% h=[5, 10, 15, 20];
k=25;
Tp = 260;
Ta = 60;
alpha = sqrt(2*h/(b*k));

syms theta(tau)
ode1 = diff(theta, tau, 2) + (tau + 1)^-1*diff(theta, tau) == alpha^2*(R-Rp)^2*(theta - 1);
% ode2 = diff(theta, tau, 2) + (tau + 1)^-1*diff(theta, tau) == 216*exp((18/5)*(x + 1));
cond1 = theta(0) == 1;
% Dtheta = diff(theta);

% cond2 = theta(1) == 1;
conds = [cond1 cond2];
ySol(tau) = dsolve(ode1, conds);
ySol = simplify(ySol)
x=linspace(0,1).';
z = ySol(x);%*(Ta-Tp)+Tp;
end


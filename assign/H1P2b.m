function H1P2b()

% Function to solve diffusion and reaction in flat catalyst geometry using
% Finite Differences method 

% Constants and discretization
Rp = 0.25; R = 0.5; b = 1/120; h = 30; k = 25; Tp = 260; Ta = 60; 
r = linspace(Rp/R,R/R,100).'; dr = r(2,1)-r(1,1);
N=size(r); N = N(1,1);
nh = 1; % number of h's to test
% Linear equation constants
beta1 = (2+dr./r); 
beta3 = (2-dr./r); 

% Construct b matrix - same for all BC's
b1 = zeros(N-2,1);
b1(1,1) = - beta3(2,1); 

% Solution matrices
% 1st BC              2nd BC              3rd BC
y1 = zeros(N-2,nh); y2 = zeros(N-2,nh); y3 = zeros(N-2,nh); % FD numerical
yf1 = zeros(N,nh); yf2 = zeros(N,nh); yf3 = zeros(N,nh); % total numerical
ya1 = zeros(N,nh); ya2 = zeros(N,nh); ya3 = zeros(N,nh); % analytical

alpha = sqrt(2*h/(b*k)); % New h and alpha
beta2 = 4 + 2*R^2*alpha^2*dr^2; % LinEq Const
        
% Prepare A matrices for all 3 BC's
A1 = zeros(N-2,N-2); % (N-2)x(N-2) matrices
    
% Construct standard A matrix A(2,:) -> A(N-3,:)
for i = 2:N-3
    A1(i,i) = -beta2;
    A1(i,i-1) = beta3(i-1,1);
    A1(i,i+1) = beta1(i+1,1);
end

% Complete all matrix constructions
A1(1,1) = -beta2; A1(1,2) = beta1(3,1);
A1(N-2,N-3) = beta3(N-2,1); A2 = A1; A3 = A1;
A1(N-2,N-2)= -beta2; 
A2(N-2,N-2) = beta1(N-1,1) - beta2; 
A3(N-2,N-2) = beta1(N-1,1)/(1+(h*R*dr/k)) - beta2;
    
% Calculate numerical solution
y1 = A1\b1; y2 = A2\b1; y3 = A3\b1; 
    
% Now find analytical solution by solving ODE
syms y(x)
Dy = diff(y);
ode = diff(y,x,2) == R^2*alpha^2*y - 1/x*Dy;
BC0 = y(0.5) == 1; % Inner BC
BC1 = y(1) == 0;   % 1st BC
BC2 = Dy(1) == 0;   % 2nd BC
BC3 = Dy(1) == -h*R*y(1)/k;   % 3rd BC

ySol1(x) = dsolve(ode,[BC0 BC1]); ySol1 = simplify(ySol1);
ySol2(x) = dsolve(ode,[BC0 BC2]); ySol2 = simplify(ySol2);
ySol3(x) = dsolve(ode,[BC0 BC3]); ySol3 = simplify(ySol3);

% Now that we have algebraic solution, calculate values for [0 1] space
ra = linspace(0.5,1,100).';
ya1 = ySol1(ra); ya2 = ySol2(ra); ya3 = ySol3(ra);

% Construct final solution y(1)->y(N)
for i=1:N
    if i == 1
        % Inner boundary
        yf1(i,:) = 1; yf2(i,:) = 1; yf3(i,:) = 1;
    elseif i == N
        % Outer boundary
        yf1(i,:) = 0; % 1st BC
        yf2(i,:) = y2(i-2,:); % 2nd BC
        yf3(i,:) = y3(i-2,:)/(1+h*R*dr/k); % 3rd BC
    else
        yf1(i,:) = y1(i-1,:); yf2(i,:) = y2(i-1,:); yf3(i,:) = y3(i-1,:);
    end
end 

% Plot
figure(2)
plot(r,yf1,'*',r,yf2,'*',r,yf3,'*','MarkerSize',3)
% hold on
% ax = gca;   % reset colors so they match up
% ax.ColorOrderIndex = 1;
% plot(ra,ya1,'-',ra,ya2,'-',ra,ya3,'-','LineWidth',2)
% hold off
ylabel('Non-dimensional Temperature, Theta = ( T-T_A ) / ( T_p-T_A )'); 
xlabel('Non-dimensional Distance, x = r/R');
ylim([0 1]); xlim([Rp/R R/R]);
legend('Num, BC1','Num, BC2','Num, BC3','Anly, BC1','Anly, BC2','Anly, BC3');
title('Effect of BC on Temperature Profile');

end
